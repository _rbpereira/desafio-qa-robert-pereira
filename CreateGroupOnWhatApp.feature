Funcionalidade: Criar um grupo no WhatsApp
	Funcionalidade destinada a testar a criação de um grupo no whatsapp.
	Testes devem ser realizados no IOS.

@Criar um grupo com sucesso
Cenario: CEN01-TC01-Criar grupo no whatsapp
	Dado Que estou com o whatsapp aberto na tela de conversas, clicar no link NOVO GRUPO
	E Selecionar as pessoas que vão fazer parte do grupo
	E Clicar no link SEGUINTE
	E No campo NOME DO GRUPO, inserir um nome para o mesmo com até 21 caracteres.
	Quando Clicar no link CRIAR
	Entao O grupo é criado exibindo o nome do grupo no topo da tela.

@Criar um grupo sem adicionar integrantes
Cenario: CEN01-TC02-Criar grupo no whatsapp sem inserir integrantes
	Dado Que estou com o whatsapp aberto na tela de conversas, clicar no link NOVO GRUPO
	Quando Estiver na tela para inserir integrantes, não selecionar nenhum.
	Entao Validar se o link SEGUINTE está desabilitado