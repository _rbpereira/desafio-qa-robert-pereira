Funcionalidade: Sair de um grupo
	Funcionalidade destinada a testar a saída de um grupo
	Testes devem ser realizados no IOS.

Cenario: CEN03-TC01-Sair de um grupo
	Dado Que estou com o whatsapp aberto na tela de conversas, clicar em cima do nome do grupo
	Quando Estiver na tela de mensagem do grupo, clicar no nome do grupo no topo da tela
	E Clicar no botão SAIR DO GRUPO exibido no rodape da pagina
	E Será exibido um modal pergutando se deseja sair da grupo. Clicar em SAIR DO GRUPO
	Entao Verificar na lista de participantes se seu nome consta na lista. Não deve constar.